`action` is a command line tool `action` for running actions using the concepts of [`prefix`](https://gitlab.com/jjlee1/prefix)

Documentation: [doc.md](./doc.md).

Like `prefix`, the action command is also based on Mark Seaborn's ideas and code
(but any bad ideas and bugs are my own).

This is a new project, interfaces are likely to change!
