# Intro

`action` is a command line tool `action` for running actions using the concepts of [`prefix`](https://gitlab.com/jjlee1/prefix)

# Concepts

See the [`prefix` documentation](https://gitlab.com/jjlee1/prefix/-/blob/master/doc.md).


An "action" is a step which will usually just run a command in an environment but can also run any Python code.

# The action command

You can think of this as a build tool that likes to do its work by constructing and running prefix commands.

The action command deals with

- commands
- actions (an action usually just runs a command in an environment, but it may run some other Python code)
- action maps (a bunch of named actions)
- environment wrappers (e.g. `in_venv` which runs a command in the virtual env named in $PWD/.venv)
- aliases for wrappers (giving new names to wrappers)

Wrappers are defined in `.action-aliases`, which is a text file
(which can be in the current working directory on in your home directory -- the former takes precedence).

Action maps are defined in `.py` files, which are Python files.
The `.py` files just list some canned commands (this may be expanded in future).
To be used, they must be referenced in an `.action-aliases` file.

## To run a command:

action [-nv] [<wrapper>...] [<command> <args>...]

e.g.

    action -v ls
    action -v in_venv ls  # runs in virtualenv named in the file $PWD/.venv
    action -v in_venv:~/env/myproject ls  # runs in virtualenv in ~/env/myproject

To run a command without printing the command first, omit `-v`:

    action ls

To just print the command without actually running it, add `-n`:

    action -nv ls

## Defining and using aliases for wrappers

Example: Create a configuration file in the current working directory named `.action-aliases` (or in `~/.action-aliases`) with this content:

    venv=in_venv:~/env/myproject

Then this:

    action -v venv ls

is shorthand for this:

    action -v in_venv:~/env/myproject ls

Aliases defined in `.action-aliases`,
as well as being used from the command line,
can be used later in the aliases file itself:

    compose_myservice=compose_env:my-service
    venv_myservice=compose_myservice in_venv

## Wrapper syntax

Wrappers on the command line and in the configuration file have the same syntax.
The syntax for a wrapper is either a simple name (e.g. `in_venv`)
or a name followed by a colon and a single string argument (e.g. `in_venv:<path to venv file>`).
A given wrappers either takes an argument or doesn't;
the two forms of `in_venv`, for example, are technically separate wrappers that happen to be spelled the same.

Aliases for wrappers never take a colon argument: they are just names.

## Combining wrappers

It is possible to combine wrappers,
either on the command line or in `.action-aliases`,
by listing them separated by whitespace:

    action -v compose_env:service in_venv ls

Or using `.action-aliases`:

    venv=compose_env:service in_venv

Then:

    action -v venv ls

## Defining prefix commands in `.action-aliases`

The `do` syntax in a configuration file is used to define an alias for a wrapper using a prefix command:

    exec_in_service=do docker-compose exec my-service

This is instead of having to add wrappers by editing the `prefix` project's source code to add to those defined out-of-the-box
(there's not currently an API to add more using Python code; I'll probably add one).

## Defining and using action maps

To define an action map, create a `.py` file listing commands
(let's assume a file in the current working directory named `build_actions.py`):

    run = ["sh", "my_script.sh"]
    black = ["black", "src"]

The action map must be referenced from an `.action-aliases` file:

    build=actions:build_actions.py

The syntax to run a named action from an action map is:

    action [-nv] [<wrapper>...] <action map> <action>

Note that instead of a full command, just the name of an action from the action map `.py` file is given on the command line.

For example, this will run the `run` command defined in `build_actions.py`:

    action -v build run

## Wrappers provided

TODO

# More details and explanation

I haven't yet exposed the `.read_cmd` feature from `prefix` in the "action map" files because I've not run into the need yet.

## Why commands and prefix commands?

See the [`prefix` documentation](https://gitlab.com/jjlee1/prefix/-/blob/master/doc.md).

## Why prefix and the action command?  Why not just ordinary prefix commands like `sudo` or `ssh`?

- Provides a place to keep a list of canned commands that can be combined with envs while remaining separate from them
  This allows for example keeping Makefiles working while still being able to easily execute development and build actions outside of make,
  and provides a way to compose commands with environments in a clean and flexible way.
- Provides a place for code to live for integration like reading docker-compose files.
  See the `compose_environ` wrapper which reads environment variables from docker-compose files.
  This example is also an example of separating commands from "envs" in the abstract sense:
  It provides a way to run commands in various configurations using the same or similar Unix environment.
- Constructing prefix commands in Python code provides a way for prefix commands to compose across machine and container boundaries
  (see for example implementations of the `cd` and `aws-gate` wrappers)
  This is possible but can be awkward using regular prefix commands because of quoting issues,
  the lack of a good way to specify on the command line the portion of a command that needs quoting,
  and the lack of wide distribution of prefix commands for common functionality.
