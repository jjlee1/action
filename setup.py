#!/usr/bin/env python

from setuptools import setup, find_packages

setup(
    name="action",
    url="https://gitlab.com/jjlee1/action",
    description="Run actions, using the concepts of pypi.org/project/prefix",
    install_requires=["prefix>=0.0.1"],
    package_dir={"": "src"},
    packages=find_packages("src"),
    platforms=["any"],
    zip_safe=True,
    entry_points={
        "console_scripts": [
            "action = action._.action:entrypoint",
        ]
    },
)
