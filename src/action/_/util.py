def ensure_trailing_slash(path: str) -> str:
    if path.endswith("/"):
        return path
    else:
        return path + "/"
