"""Tool to run a command or a named action in an environment.

Usage:
action [-nv] [<wrapper>...] [<command> <args>...]
action [-nv] [<wrapper>...] <action map> <action>

You can think of this as a build tool that likes to do its work by constructing
and running prefix commands.

Documentation is here: https://gitlab.com/jjlee1/action/doc.md
"""

from collections.abc import Callable, Mapping
from typing import Any, NoReturn, Optional, TypeVar, Union, cast
import argparse
import functools
import io
import os
import subprocess
import sys

from prefix import (
    Cmd,
    Env,
    PrefixCmdEnv,
    VerboseWrapper,
    add_basic_env_arguments,
    get_env_from_arguments,
    in_dir,
    shell_escape,
)

# TODO export these types from prefix
CmdKwds = Any
Process = Union[subprocess.CompletedProcess[Any], subprocess.Popen[Any], NoReturn]
Wrapper = Callable[[Env], Env]

Environ = Mapping[str, str]
Action = Callable[[Env], None]
ActionMap = dict[str, Action]
Item = Union[Wrapper, ActionMap]
Aliases = dict[str, Item]


T = TypeVar("T")
Returning = Callable[..., T]


def memoize(fn: Returning[T]) -> Returning[T]:
    Missing = object()
    memo: dict[tuple[Any, ...], Any] = {}
    def wrapper(*args: Any) -> T:
        val = memo.get(args, Missing)
        if val is Missing:
            val = fn(*args)
            memo[args] = val
        return cast(T, val)
    return wrapper


@memoize
def read_actions_config_file(path: str) -> Optional[ActionMap]:
    if os.path.exists(path):
        environ: dict[Any, Any] = {}
        with open(path) as fh:
            exec(fh.read(), environ)
        commands: dict[str, Cmd] = dict(
            (k, v)
            for k, v in environ.items()
            if isinstance(v, list)
        )
        map_ = {}
        for name, cmd in commands.items():
            def make_action(cmd: Cmd) -> Callable[[Env], None]:
                def action(env: Env) -> None:
                    env.cmd(cmd, nocommunicate=True)
                return action
            map_[name] = make_action(cmd)
        return ActionMap(map_)
    else:
        return None


def get_action_map(spec: str) -> Optional[ActionMap]:
    key = "actions:"
    if spec.startswith(key):
        path = spec.removeprefix(key)
        return read_actions_config_file(path)
    else:
        return None


def parse_arguments(prog: str, args: Cmd) -> argparse.Namespace:
    parser = argparse.ArgumentParser(prog=prog)
    add_basic_env_arguments(parser.add_argument)
    arguments, remaining_args = parser.parse_known_args(args)
    arguments.remaining_args = remaining_args
    return arguments


def combine_wrappers(wrappers: list[Wrapper]) -> Wrapper:
    def combined_wrapper(env: Env) -> Env:
        for wrapper in wrappers:
            env = wrapper(env)
        return env

    return combined_wrapper


def prefix_wrapper(prefix_command: Cmd) -> Wrapper:
    def make(env: Env) -> Env:
        return PrefixCmdEnv(prefix_command, env)

    return make


def docker_compose_exec(service: str) -> Wrapper:
    return prefix_wrapper(["docker-compose", "exec", service])


def docker_compose_run(service: str) -> Wrapper:
    return prefix_wrapper(["docker-compose", "run", service])


class ShellEscapeEnv:
    def __init__(self, env: Env):
        self._env = env

    def cmd(self, args: Cmd, **kwargs: CmdKwds) -> Process:
        return self._env.cmd([shell_escape(args)], **kwargs)


def aws_gate(instance_id: str) -> Wrapper:
    def make(env: Env) -> Env:
        # TODO: get id from human-understandable string
        return ShellEscapeEnv(PrefixCmdEnv(["aws-gate", "exec", instance_id], env))
    return make


def docker_compose_environ(service: str) -> Wrapper:
    import yaml

    def load_environ(fh: io.TextIOBase) -> dict[str, str]:
        config = yaml.safe_load(fh)["services"][service]
        environ: dict[str, str] = {}
        path = config.get("env_file")
        if path is not None:
            with open(path) as lines:
                environ.update(dict(l.rstrip("\n").split("=", 1) for l in lines if l and not l.startswith("#")))
        # `environment` takes precedence over `env_file`
        environment = config.get("environment")
        if environment is not None:
            environ.update(dict(l.split("=", 1) for l in environment))
        return environ

    def make(env: Env) -> Env:
        with open("docker-compose.yml") as compose, \
             open("docker-compose.override.yml") as override:
            environ = load_environ(compose)
            environ.update(load_environ(override))
            # This could be done in python rather than using env but this
            # composes with any prefix command so that it works even under say
            # aws_gate or in docker
            return PrefixCmdEnv(["env"] + [f"{k}={v}" for k, v in environ.items()], env)

    return make


def in_venv(venv_path: str) -> Cmd:
    # It would be nice to use a short command to do this (such as command
    # `venv` distributed in jjlee1/bash_prefix_commands on gitlab), but many
    # simple prefix commands such as this are not widely distributed under
    # well-known names.  That means that even if they were included in this
    # package, they would not compose with wrappers such as aws_gate.
    return ["sh", "-c", '. "$1"/bin/activate && shift && exec "$@"', "inline_venv", venv_path]


WRAPPER_FACTORIES: dict[str, Callable[[str], Wrapper]] = {
    "cd": lambda dir_path: prefix_wrapper(in_dir(dir_path)),
    "compose_exec": docker_compose_exec,
    "compose_run": docker_compose_run,
    "aws_gate": aws_gate,
    "sudo": lambda user: prefix_wrapper([
        "sudo", "-p", "Password to get from %u to %U on %H: ", "-u", 
        user]),
    "plainsudo": lambda user: prefix_wrapper(["sudo", user]),
    # wrapper "prefix" only works with length-one prefix commands (sudo as a
    # simple "sudo") -- for multiple-argument prefix commands, use the "do"
    # configuration syntax instead
    "prefix": lambda prefix: prefix_wrapper([prefix]),
    "compose_environ": docker_compose_environ,
    "in_venv": lambda venv_path: prefix_wrapper(in_venv(os.path.expanduser(venv_path))),
}


def in_default_venv(env: Env) -> Env:
    with open(".venv") as fh:
        path = fh.read().rstrip("\n")
        return PrefixCmdEnv(in_venv(path), env)


WRAPPER_ALIASES: dict[str, Wrapper] = {
    "verbose": VerboseWrapper,
    "in_home": prefix_wrapper(["sh", "-c", 'cd && exec "$@"', "-"]),
    "in_venv": in_default_venv,
}


def get_item_by_name(name: str, aliases: Aliases) -> Item:
    if ":" in name:
        map_ = get_action_map(name)
        if map_ is None:
            type_name, arg = name.split(":", 1)
            return WRAPPER_FACTORIES[type_name](arg)
        else:
            return map_
    elif name in aliases:
        return aliases[name]
    else:
        raise KeyError(name)


def get_action_map_by_name(name: str, aliases: Aliases) -> Optional[ActionMap]:
    try:
        item = get_item_by_name(name, aliases)
    except KeyError:
        return None
    if not callable(item):
        # ActionMap
        return item
    else:
        # Wrapper
        return None


def wrapper_from_args(args: Cmd, aliases: Aliases) -> Wrapper:
    wrappers = []
    while len(args) > 0:
        name = args.pop(0)
        if name == "do":
            wrappers.append(prefix_wrapper(args))
            break
        else:
            item = get_item_by_name(name, aliases)
            if not callable(item):
                raise Exception(f"Mixing wrappers with actions in alias {name}")
            wrappers.append(item)
    return combine_wrappers(wrappers)


def item_from_args(args: Cmd, aliases: Aliases) -> Item:
    map_ = get_action_map_by_name(args[0], aliases)
    if map_ is not None:
        return map_
    else:
        return wrapper_from_args(args, aliases)


def read_config(path: str, aliases: Aliases) -> None:
    if not os.path.exists(path):
        return

    for line in open(path, "r"):
        line = line.strip()
        if not line or line.startswith("#"):
            continue
        if "=" not in line:
            raise Exception(f"Bad config line: {line!r}")
        name, sep, args_text = line.partition("=")
        if sep == "=":
            args = args_text.strip().split()
        else:
            args = []
        name = name.strip()
        if name in aliases:
            print(f"warning: {name!r} is being replaced")
        aliases[name] = item_from_args(args, aliases)


def main(prog: str, args: Cmd) -> int:
    aliases: dict[str, Item] = dict(WRAPPER_ALIASES)

    for path in [os.path.expanduser("~/.action-aliases"), ".action-aliases"]:
        read_config(path, aliases)

    arguments = parse_arguments(prog, args)
    args = arguments.remaining_args
    env = get_env_from_arguments(arguments)
    action_map = None
    wrappers: list[Wrapper] = []
    while True:
        if len(args) == 0:
            args = ["bash"]
            break
        else:
            name = args[0]
            try:
                item = get_item_by_name(name, aliases)
            except KeyError:
                break
            else:
                args.pop(0)
                if callable(item):
                    wrappers.append(item)
                else:
                    action_map = item
                    break
    env = combine_wrappers(wrappers)(env)
    if action_map is not None:
        action = action_map[args[0]]
    else:
        def action(env: Env) -> None:
            env.cmd(args, nocommunicate=True)
    try:
        action(env)
    except subprocess.CalledProcessError as exc:
        return exc.returncode
    return 0


def entrypoint() -> None:
    sys.exit(main(sys.argv[0], sys.argv[1:]))


if __name__ == "__main__":
    entrypoint()
